import {taskHandler} from "./controller.js";

self.onmessage = function ({data}) {
    taskHandler.dispatch(data.type, data.args)
        .then(res => {
            self.postMessage({
                msg: 'success',
                data: res
            });
        })
        .catch(err => {
            self.postMessage({
                msg: 'fail',
                data: err.message,
            });
        });
};

self.postMessage('ready');
