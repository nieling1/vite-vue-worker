const taskHandler = {
    list: {},
    register(type, handler) {
        this.list[type] = handler;
        return this;
    },
    dispatch(type, args) {
        return new Promise((resolve, reject) => {
            return this.list[type](resolve, reject, args);
        });
    }
};

taskHandler
    .register('task1', function (resolve, reject, args) {
        setTimeout(() => {
            resolve(args.msg + ' success');
        }, args.time);
    })
    .register('task2', function (resolve, reject, args) {
        // resolve(args);
    });

export {taskHandler}
