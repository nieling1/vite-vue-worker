## 用于vite的worker多线程任务调度，代码很少，开箱即用
### 一、将worker目录复制到你的项目
### 二、在worker/controller.js中注册任务，将任务处理结果通过resolve（成功）或reject（失败）返回,args是任务参数。支持异步
![img_1.png](img_1.png)
### 三、在你的项目中引入poll线程池就可以开始执行任务了。可以传递参数
![img.png](img.png)
### 四、默认开启cpu核心数-1个线程，如果需要更改可以在core.js中修改
![img_3.png](img_3.png)
